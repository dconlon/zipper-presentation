# Zipper

Slides and code from my ScalaSyd presentation.

For the code just run sbt in the directory and import the Zip object.

References appended for the curious.

Forgive the lack of rigour througout.

Thanks to reveal.js.

Slides & code are covered under http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
