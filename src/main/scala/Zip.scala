object Zip {
  abstract class BTree[A]
  case class Leaf[A](value: A) extends BTree[A]
  case class Node[A](value: A, left: BTree[A], right: BTree[A]) extends BTree[A]

  abstract class Decision[T]
  case class Left[T](value: T) extends Decision[T]
  case class Right[T](value: T) extends Decision[T]

  case class ZippedTree[T](path: List[Decision[BTree[T]]], focus: BTree[T]) {
    def goLeft(): ZippedTree[T] = focus match {
      case Leaf(_) => throw new Exception("Can't move left")
      case Node(_, l, _) => ZippedTree(Left(focus) :: path, l)
    }
    def goRight(): ZippedTree[T] = focus match {
      case Leaf(_) => throw new Exception("Can't move right")
      case Node(_, _, r) => ZippedTree(Right(focus) :: path, r)
    }
    def goUp(): ZippedTree[T] = path match {
      case Nil => throw new Exception("Can't go up")
      case Left(Node(v, _, r)) :: rest =>
        ZippedTree(rest, Node(v, focus, r))
      case Right(Node(v, l, _)) :: rest =>
        ZippedTree(rest, Node(v, l, focus))
    }
    def update(f: BTree[T] => BTree[T]): ZippedTree[T] =
      ZippedTree(path, f(focus))
    def zipUp(): BTree[T] = {
      def rollup(z: ZippedTree[T]): BTree[T] = z match {
        case ZippedTree(Nil, f) => f
        case x => rollup(x.goUp())
      }
      rollup(this)
    }
  }

   case class ZippedList[T](before: List[T], focus: T, after: List[T]) {
     def goLeft(): ZippedList[T] = before match {
       case Nil => throw new Exception("Can't move left")
       case x::xs => ZippedList(xs,x,focus::after)
     }
     def goRight(): ZippedList[T] = after match {
       case Nil => throw new Exception("Can't move right")
       case x::xs => ZippedList(focus::before,x,xs)
     }
     def update(f: T => T): ZippedList[T] = ZippedList(before,f(focus),after)
     def zipUp(): List[T] = (focus :: before).reverse ++ after
   }

  val t: BTree[Int] = Node(1,
    Node(7,
      Leaf(3),
      Leaf(4)),
    Node(7,
      Node(7,
        Leaf(9),
        Leaf(2)),
      Leaf(4)))
}
